class RegularShape{
    constructor(noSides, length){
        if(this.getPerimeter===undefined){
            throw new Error("Class must implement getPerimeter() method")
        }
        if(this.getArea===undefined){
            throw new Error("Class must implement getArea() method")
        }
        this.noSides=noSides
        this.length=length
    }
}
class Square extends RegularShape{
    getPerimeter(){
        return `The perimeter of the square is ${this.noSides*this.length}`
    }
    getArea(){
        return `The area of the square is ${this.length**2}`
    }
}

// let square1 = new Square(4,12)
// console.log(square1.getPerimeter())
// console.log(square1.getArea())
const shape1 = new Square(4,16)
console.log(shape1.getPerimeter())
console.log(shape1.getArea())

class Food{
    constructor(name, price){
        if(this.getName===undefined){
            throw new Error("Class must implement getName() method")
        }
        this.name=name
        this.price=price
    }
}

class Vegetable extends Food{
    constructor(name, breed, price){
        super(name, price)
        this.breed=breed
    }
    getName(){
        return `${this.name} is of ${this.breed} variety and is priced at ${this.price} pesos.`
    }
}

const vegetable1 = new Vegetable('Pechay', 'Native', 25)
console.log(vegetable1.getName())

class Equipment{
    constructor(equipmentType){
        if(this.printInfo===undefined){
            throw new Error("Class must implement printInfo() method")
        }
        this.equipmentType=equipmentType
    }
}
class Bulldozer extends Equipment{
    constructor(equipmentType, model, bladeType){
        super(equipmentType)
        this.model=model
        this.bladeType=bladeType
    }
    printInfo(){
        return `Info: ${this.equipmentType}\nThe bulldozer ${this.model} has a ${this.bladeType} blade`
    }
}
class TowerCrane extends Equipment{
    constructor(equipmentType, model, hookRadius, maxCapacity){
        super(equipmentType)
        this.model=model
        this.hookRadius=hookRadius
        this.maxCapacity=maxCapacity
    }
    printInfo(){
        return `Info: ${this.equipmentType}\nThe tower crane ${this.model} has ${this.hookRadius} cm hook radius and ${this.maxCapacity} kg max capacity`
    }
}
let bulldozer1 = new Bulldozer("bulldozer", "Brute", "Shovel")
console.log(bulldozer1.printInfo())
let towercrane1 = new TowerCrane("tower crane", "Pelican", 100, 1500)
console.log(towercrane1.printInfo())

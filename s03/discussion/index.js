// Abstraction
    // creating a simplified model/blueprint
    // It helps us focus on what's important and ignore the complicated details

class Person{
    constructor(){
        if(this.constructor === Person){
            throw new Error(
                "Object cannot be created from an abstract class Person"
            )
        }
        if(this.getFullName === undefined){
            throw new Error(
                "Class must implement getFullName() method"
            )
        }
        if(this.getFirstName === undefined){
            throw new Error(
                "Class must implement getFirstName() method"
            )
        }
        if(this.getLastName === undefined){
            throw new Error(
                "Class must implement getLastName() method"
            )
        }
    }
}

class Emmployee extends Person{
    constructor(firstName, lastName, employeeId){
        super()
        this.firstName = firstName
        this.lastName = lastName
        this.employeeId = employeeId
    }
    getFullName(){
        return `${this.firstName} ${this.lastName} has employee ID of ${this.employeeId}`
    }
    getFirstName(){
        return this.firstName
    }
    getLastName(){
        return this.lastName
    }
}

const empployeeA = new Emmployee('John', 'Smith', 'em4')
console.log(empployeeA.getFirstName())
console.log(empployeeA.getLastName())
console.log(empployeeA.getFullName())

class Student extends Person{
    constructor(firstName, lastName, studentId, section){
        super()
        this.firstName = firstName
        this.lastName = lastName
        this.studentId = studentId
        this.section = section
    }
    getFullName(){
        return `${this.firstName} ${this.lastName} has student ID of ${this.studentId}`
    }
    getFirstName(){
        return this.firstName
    }
    getLastName(){
        return this.lastName
    }
    getStudentDetails(){
        return `${this.firstName} ${this.lastName} has student ID of ${this.studentId} belongs to the ${this.section} section`
    }
}

const student1 = new Student('Brandon', 'Boyd', 'SID-1100', 'Mango')
const student2 = new Student('Jeff', 'Cruz', 'SID-1101', 'Narra')
const student3 = new Student('Jane', 'Doe', 'SID-1102', 'Sampaguita')
console.log(student1.getStudentDetails())
console.log(student2.getStudentDetails())
console.log(student3.getStudentDetails())
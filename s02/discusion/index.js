// Polymorphism
    // involves the concept of overriding, where method in subclass overrides the implementation of a method with the same name in its superclass

class Person{
    constructor(firstName, lastName){
        this.firstName= firstName
        this.lastName= lastName
    }
    getFullName(){
        return `This person's name is ${this.firstName} ${this.lastName}`
    }
}

class Employee extends Person{
    constructor(firstName, lastName, employeeId){
        super(firstName, lastName)
        this.employeeId = employeeId
    }
    getEmployeeDetails(){
        return `The ID ${this.employeeId} belongs to ${this.getFullName()}`
    }
    getFullName(){
        return super.getFullName() + ` with employeeId ${this.employeeId}`
    }
}

const employeeA = new Employee('John', 'Smith', 'EM-004')
console.log(employeeA.getFullName())

class TeamLead extends Employee{
    constructor(firstName, lastName, employeeId){
        super(firstName, lastName, employeeId)
        this.teamMembers = []
    }
    addTeamMember(employee){
        this.teamMembers.push (employee)
        return this
    }
    getTeamMembers(){
        this.teamMembers.forEach(m=> console.log(m.getFullName()))
        return this
    }
    getFullName(){
        return super.getFullName() + ` and s/he is a team lead`
    }
}
const employeeB = new TeamLead('Erwin', 'Smith', 'EM-005')
console.log(employeeB.getFullName())
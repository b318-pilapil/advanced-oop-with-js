
class Person{
    constructor(){
        if(this.constructor === Person){
            throw new Error(
                "Object cannot be created from an abstract class Person"
            )
        }
        if(this.getFullName === undefined){
            throw new Error(
                "Class must implement getFullName() method"
            )
        }
    }
}

class Employee extends Person{
    // Encapsulation
        // adided by private fields (#), ensures data protection
        // Setters and getters prvide controlled acess to encapsulated data
    #firstName
    #lastName
    #employeeId
    constructor(firstName, lastName, employeeId){
        super()
        this.#firstName = firstName
        this.#lastName = lastName
        this.#employeeId = employeeId
    }
    getFullName(){
        return `${this.#firstName} ${this.#lastName} has employee ID of ${this.#employeeId}`
    }
    getFirstName(){
        return this.#firstName
    }
    getLastName(){
        return this.#lastName
    }
    getEmployeeId(){
        return this.#employeeId
    }
    setFirstName(firstName){
        this.#firstName=firstName
    }
    setLastName(lastName){
        this.#lastName=lastName
    }
    setEmployeeId(employeeId){
        this.#employeeId=employeeId
    }
}

const employeeA = new Employee('John', 'Smith', 'EM-01')
console.log(employeeA.getFirstName())
employeeA.setFirstName("David")
console.log(employeeA.getFirstName())
console.log(employeeA.getFullName())

const employeeB = new Employee()
console.log(employeeB.getFullName())
employeeB.setFirstName("Jill")
employeeB.setLastName("Hill")
employeeB.setEmployeeId("EM-002")
console.log(employeeB.getFullName())
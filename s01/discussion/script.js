// Inheritance
    // refers to the mechanism by which one class can inherit properties and methods from another class
class Person{
    constructor(firstName, lastName){
        this.firstName = firstName
        this.lastName = lastName
    }
    getFullName(){
        return `${this.firstName} ${this.lastName}`
    }
}
const person1 = new Person('John', 'Smith')
console.log(person1)
console.log(person1.getFullName())

const person2 = new Person('John', 'Doe')
console.log(person2)
console.log(person2.getFullName())

class Employee extends Person{
    constructor(employeeId, firstName, lastName){
        super(firstName, lastName)
        this.employeeId = employeeId
    }
    getEmployeeDetails(){
        // return `The ID ${this.employeeId} belongs to ${this.firstName} ${this.lastName}`
        return `The ID ${this.employeeId} belongs to ${this.getFullName()}`
    }
}
const employee1 = new Employee("Acme-001", 'John', 'Roberts')
console.log(employee1)
console.log(employee1.getFullName())
console.log(employee1.getEmployeeDetails())

class TeamLead extends Employee{
    constructor(employeeId, firstName, lastName){
        super(employeeId, firstName, lastName)
        this.teamMembers = []
    }
    addTeamMember(employee){
        this.teamMembers.push (employee)
        return this
    }
    getTeamMembers(){
        this.teamMembers.forEach(m=> console.log(m.getFullName()))
        return this
    }
}

const teamlead = new TeamLead("Acme-002", "Leri", "Medina")
console.log(teamlead)
console.log(teamlead.getFullName())
console.log(teamlead.getEmployeeDetails())

const employee2 = new Employee("Acme-003", 'Brandon', 'Smith')
const employee3 = new Employee("Acme-004", 'Jobert', 'Pakundangan')
const employee4 = new Employee("Acme-005", 'Jhun Jhun', 'Dela Cruz')

teamlead.addTeamMember(employee1)
    .addTeamMember(employee2)
    .addTeamMember(employee3)
    .addTeamMember(employee4)
    .getTeamMembers()

    
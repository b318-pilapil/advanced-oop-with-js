class Contractor{
    constructor(name, email, contactNo){
        this.name=name
        this.email=email
        this.contactNo=contactNo
    }
    getContractorDetails(){
        console.log(`Name: ${this.name}`)
        console.log(`Email: ${this.email}`)
        console.log(`Contact No: ${this.contactNo}`)
    }
}

let contractor1= new Contractor("Ultra Manpower Services", "ultra@manpower.com", "09167890123")
contractor1.getContractorDetails()

class SubContractor extends Contractor{
    constructor(name, email, contactNo, specializatons){
        super(name, email, contactNo)
        this.specializatons = specializatons
    }
    getSubConDetails(){
        this.getContractorDetails()
        console.log(this.specializatons.join(','))
    }
}

let subCon1 = new SubContractor("Ace Bros", "acebros@mail.com", "09151234567", ['gardens', 'industrial'])
let subCon2 = new SubContractor("LitC corp", "litc@mail.com", "091212456789", ['schools', 'hospitals', 'bakeries', 'libraries'])
subCon1.getSubConDetails()
subCon2.getSubConDetails()
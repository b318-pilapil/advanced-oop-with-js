class Request{
    #requesterEmail
    #content
    #dateRequested
    constructor(requesterEmail, content){
        this.#requesterEmail = requesterEmail
        this.#content = content
        this.#dateRequested = new Date()
    }
    getRequesterEmail(){
        return this.#requesterEmail
    }
    getContent(){
        return this.#content
    }
    getDateRequested(){
        return this.#dateRequested
    }
    setFirstName(requesterEmail){
        this.#requesterEmail=requesterEmail
    }
    setContent(content){
        this.#content=content
    }
    setDateRequested(dateRequested){
        this.#dateRequested=dateRequested
    }
}

class Person{
    constructor(){
        if(this.constructor === Person){
            throw new Error("Object cannot be created from an abstract class Person")
        }
        if(this.getFullName === undefined){
            throw new Error("Class must implement getFullName() method")
        }
        if(this.login === undefined){
            throw new Error("Class must implement login() method")
        }
        if(this.logout === undefined){
            throw new Error("Class must implement logout() method")
        }
        if(this.constructor === Employee && this.addRequest === undefined){
            throw new Error("Class must implement addRequest() method")
        }
        if(this.constructor === TeamLead && this.addMember === undefined){
            throw new Error("Class must implement addMember() method")
        }
        if(this.constructor === TeamLead && this.checkRequests === undefined){
            throw new Error("Class must implement checkRequests() method")
        }
        if(this.constructor === Admin && this.addTeamLead === undefined){
            throw new Error("Class must implement addTeamLead() method")
        }
        if(this.constructor === Admin && this.deactivateTeam === undefined){
            throw new Error("Class must implement deactivateTeam() method")
        }
    }
}

class Employee extends Person{
    #firstName
    #lastName
    #email
    #department
    #isActive
    #requests
    constructor(firstName, lastName, email, department){
        super()
        this.#firstName = firstName
        this.#lastName = lastName
        this.#email = email
        this.#department = department
        this.#isActive = true
        this.#requests = []
    }
    getFirstName(){
        return this.#firstName
    }
    getLastName(){
        return this.#lastName
    }
    getEmail(){
        return this.#email
    }
    getDepartment(){
        return this.#department
    }
    getIsActive(){
        return this.#isActive
    }
    getRequests(){
        return this.#requests
    }
    setFirstName(firstName){
        this.#firstName=firstName
    }
    setLastName(lastName){
        this.#lastName=lastName
    }
    setEmail(email){
        this.#email=email
    }
    setDepartment(department){
        this.#department=department
    }
    setIsActive(isActive){
        this.#isActive=isActive
    }
    setRequests(requests){
        this.#requests=requests
    }
    
    getFullName(){
        return `${this.#firstName} ${this.#lastName} with the email ${this.#email}`
    }
    login(){
        return `${this.#firstName} has logged in`
    }
    logout(){
        return `${this.#firstName} has logged out`
    }
    addRequest(content){
        this.#requests.push(new Request(this.#email, content))
    }
}
class TeamLead extends Person{
    #firstName
    #lastName
    #email
    #department
    #isActive
    #members
    constructor(firstName, lastName, email, department){
        super()
        this.#firstName = firstName        
        this.#lastName = lastName
        this.#email = email
        this.#department = department
        this.#isActive = true
        this.#members = []
    }
    getFirstName(){
        return this.#firstName
    }
    getLastName(){
        return this.#lastName
    }
    getEmail(){
        return this.#email
    }
    getFullName(){
        return `${this.#firstName} ${this.#lastName} with the email ${this.#email}`
    }
    getDepartment(){
        return this.#department
    }
    getIsActive(){
        return this.#isActive
    }
    getMembers(){
        return this.#members
    }
    setFirstName(firstName){
        this.#firstName=firstName
    }
    setLastName(lastName){
        this.#lastName=lastName
    }
    setEmail(email){
        this.#email=email
    }
    setDepartment(department){
        this.#department=department
    }
    setIsActive(isActive){
        this.#isActive=isActive
    }
    setMembers(members){
        this.#members=members
    }

    getFullName(){
        return `Team Lead ${this.#firstName} ${this.#lastName} with the email ${this.#email}`
    }
    login(){
        return `Team Lead ${this.#firstName} has logged in`
    }
    logout(){
        return `Team Lead ${this.#firstName} has logged out`
    }
    addMember(employee){
        if(Object.getPrototypeOf(employee)===Employee.prototype){ //allow only Employee instances
            if(!this.#members.includes(employee)) //prevent duplicate
                this.#members.push(employee)
            return this //for chaining
        }else{
            return `Passed variable is not an instance of class Employee\nVariable: ${employee}`
        }
    }
    checkRequests(employeeEmail){
        const i = this.#members.findIndex(e => e.getEmail() === employeeEmail)
        if(i > -1){
            return this.#members[i].getRequests()
        }else{
            return `Member with email ${employeeEmail} not found.`
        }
    }
}
class Admin extends Person{
    #firstName
    #lastName
    #email
    #department
    #teamLeads
    constructor(firstName, lastName, email, department){
        super()
        this.#firstName = firstName        
        this.#lastName = lastName
        this.#email = email
        this.#department = department
        this.#teamLeads = []
    }
    getFirstName(){
        return this.#firstName
    }
    getLastName(){
        return this.#lastName
    }
    getEmail(){
        return this.#email
    }
    getDepartment(){
        return this.#department
    }
    getTeamLeads(){
        return this.#teamLeads
    }
    setFirstName(firstName){
        this.#firstName=firstName
    }
    setLastName(lastName){
        this.#lastName=lastName
    }
    setEmail(email){
        this.#email=email
    }
    setDepartment(department){
        this.#department=department
    }
    setTeamLeads(teamLeads){
        this.#teamLeads=teamLeads
    }

    getFullName(){
        return `Admin ${this.#firstName} ${this.#lastName} with the email ${this.#email}`
    }
    login(){
        return `Admin ${this.#firstName} has logged in`
    }
    logout(){
        return `Admin ${this.#firstName} has logged out`
    }
    
    addTeamLead(teamlead){
        if(Object.getPrototypeOf(teamlead)===TeamLead.prototype){ //allow only Employee instances
            if(!this.#teamLeads.includes(teamlead)) //prevent duplicate
                this.#teamLeads.push(teamlead)
            return this //for chaining
        }else{
            return `Passed variable is not an instance of class TeamLead\nVariable: ${teamlead}`
        }
    }
    deactivateTeam(teamLeadEmail){
        const i = this.#teamLeads.findIndex(e => e.getEmail() === teamLeadEmail)
        if(i > -1){
            if(this.#teamLeads[i].getIsActive()===true){
                this.#teamLeads[i].setIsActive(false)
                this.#teamLeads[i].setMembers([])
                return(`Team with Lead ${teamLeadEmail} has been deactivated.`)
            }else{
                return(`Team with Lead ${teamLeadEmail} is already deactivated.`) //prevent redundancy
            }
        }else{
            return `Team Lead with email ${teamLeads} not found.`
        }
    }
}

console.warn('Employee section')
const e1 = new Employee('e1', 'e1', 'e1@mail.com', 'd1')
const e2 = new Employee('e2', 'e2', 'e2@mail.com', 'd1')
const e3 = new Employee('e3', 'e3', 'e3@mail.com', 'd2')
const e4 = new Employee('e4', 'e4', 'e4@mail.com', 'd2')

console.log(e1.getFullName())
console.log(e1.getRequests())
e1.addRequest('i want food')
console.log(e1.getRequests())
e1.addRequest('i want water')
e2.addRequest('i want food too')
e3.addRequest('i want food three')
e4.addRequest('i want water too')
console.log(e2.getRequests())
console.log(e3.getRequests())
console.log(e4.getRequests())

console.warn('Team Lead section')
const t1 = new TeamLead('t1', 't1', 't1@mail.com', 'd1')
const t2 = new TeamLead('t2', 't2', 't2@mail.com', 'd2')

console.log(t1.getMembers())
console.log(t1.addMember(e1).addMember(e2).addMember(e3))
console.log(t1.getMembers())
console.log(t1.getIsActive())
console.log(t2.addMember(e3).addMember(e4))
console.log(t2.getMembers())
console.log(t2.getIsActive())

console.log(t1.addMember({a:'a', b: 'b'})) //meant to fail

console.log('')
t1.getMembers().forEach(e=>{
    console.log(t1.checkRequests(e.getEmail()))
})
console.log(t1.checkRequests('dummy@mail.com')) //meant to fail

console.warn('Admin section')
const a1 = new Admin('a1', 'a1', 'a1@mail.com', 'd1')
console.log(a1.getTeamLeads())
a1.addTeamLead(t1).addTeamLead(t2)
console.log(a1.getTeamLeads())

console.log(a1.addTeamLead({a:'a', b: 'b'})) //meant to fail

console.log('')
console.log(a1.deactivateTeam(t1.getEmail()))
console.log(t1.getIsActive())
console.log(t1.getMembers())

console.log(a1.deactivateTeam(t1.getEmail()))

console.log(t1.checkRequests('dummy@mail.com')) //meant to fail